{{--
  Template Name: Maintenance Template
--}}
<!doctype html>
<html {!! get_language_attributes() !!}>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        Under Maintenace
    </title>
    @php wp_head() @endphp
    </head>
  <body @php body_class() @endphp>
    <div class="row maintenance-container">
        <!-- <h5>The site is under maintenance but should be back online soon.</h5>
        <p>
            Meanwhile, you can visit <a class="default-btn shop-directory-btn" href="https://shop.hehe.rw/">
            shop.hehe.rw</a> 
            to learn more about us
        </p> -->
        <div style="width: 100%;margin:0 auto;text-align:center;">
        <form role="form" action="//track.trackingmore.com" method="get" onsubmit="return false">
            <div class="TM_input-group">
                <input type="text" class="TM_my_search_input_style " id="button_tracking_number" placeholder="Tracking Number" name="button_tracking_number" value="" autocomplete="off" maxlength="100" style="border-color: #0099cc">
                <span class="TM_input-group-btn">
                    <button class="TM_my_search_button_style " id="query" type="button" onclick="return doTrack()" style="background-color: #0099cc">Track</button>
                </span>
            </div>
            <input type="hidden" name="lang" value="en" />
            <input id="button_express_code" type="hidden" name="lang" value="" />
        </form>
        <div id="TRNum"></div>
      </div>
                                                        
    </div> 
    <script type="text/javascript" src="//s.trackingmore.com/plugins/v1/buttonCurrent.js"></script>   
    @php do_action('get_footer') @endphp
    @php wp_footer() @endphp
  </body>
</html>
