{{--
  Template Name: Home Template
--}}
<?php
    // header content data
    $menu_items = get_field('menu_item');
    
    // top content data
    $welcome_text_content = get_field('welcome_text');
    $top_section_subtitle = get_field('subtitle');
    $top_section_button = get_field('get_started_button');
    $top_action_buttons = get_field('action_buttons');

    // simple container data
    $steps = get_field('steps');
    $simple_section_title = get_field('simple_section_title');
    $simple_section_subtitle = get_field('simple_section_subtitle');
    $simple_section_contact_text = get_field('contact_text');
    $simple_section_button_text = get_field('button_section_text');

    // shop container data
    $products = get_field('product_slider');

    $product_section_title_one = get_field('product_section_title_one');
    $product_section_title_two = get_field('product_section_title_two');
    $product_section_subtitle = get_field('product_section_subtitle');
    $product_section_view_more_btn = get_field('view_more_button');
    $product_section_get_started_btn = get_field('shop_get_started_button');

    // contact us section data
    $contact_us_title = get_field('contact_us_title');
    $contact_us_subtitle = get_field('contact_us_subtitle');
    $submit_button_text = get_field('submit_button_text');
    $submission_success_message = get_field('submission_success_text');
    $submission_failure_message = get_field('submission_failure_text');
    $contact_us_form_fields = get_field('form_fields');
    $industry_field_data = get_field('client_industries');
    $contact_email_receivers = get_field('contact_email_receivers');
    
    // footer section data
    $footer_section_data = get_field('section_contact_item');
    $social_media_contacts = get_field('social_media_contacts');

?>
<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111662450-2"></script>
  <body @php body_class() @endphp>
   
    @include ('partials.state_indicator')
    
    @include('partials.top_section', [
    'welcome_text_content' => $welcome_text_content,
    'subtitle' => $top_section_subtitle,
    'section_button' => $top_section_button,
    'action_buttons' => $top_action_buttons,
    'menu_items' => $menu_items
    ])

    
    @include('partials.simple_section', [
    'steps' => $steps,
    'title' => $simple_section_title,
    'subtitle' => $simple_section_subtitle,
    'contact_text' => $simple_section_contact_text,
    'button_text' => $simple_section_button_text
    ])

    @include('partials.shop', [
      'products' => $products,
      'title_one' => $product_section_title_one,
      'title_two' => $product_section_title_two,
      'subtitle' => $product_section_subtitle,
      'view_more_btn' => $product_section_view_more_btn,
      'get_started_btn' => $product_section_get_started_btn
    ])



    @include('partials.client')

    @include('partials.contact_us', [
      'title' => $contact_us_title,
      'subtitle' => $contact_us_subtitle,
      'submit_button_text' => $submit_button_text,
      'form_fields' => $contact_us_form_fields,
      'industry_data' => $industry_field_data,
      'email_receivers' => $contact_email_receivers,
      'success_message' => $submission_success_message,
      'error_message' => $submission_failure_message
    ])
    

    @php do_action('get_footer') @endphp
    @include('partials.footer', [
    'footer_section_data' => $footer_section_data,
    'social_media_contacts' => $social_media_contacts
    ])
    @php wp_footer() @endphp
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <!-- JavaScript -->
    <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
    <script src="https://res.cloudinary.com/hehe/raw/upload/v1551195415/cart-site/libraries/jquery.waypoints.min.js"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-111662450-2');
    </script>
  </body>
</html>
