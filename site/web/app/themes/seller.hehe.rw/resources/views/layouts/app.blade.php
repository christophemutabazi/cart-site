{{--
  Template Name: Homepapage Template
--}}
<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    
    <div class="row top-container">
        @include('partials.header')
        @php
          print_r('<hr />');
          $field = get_field('welcome_text');
          print_r($field);
          print_r('<hr />');
          die();
        @endphp
        <div class="row jumbotron-container">
              <div class="row center-container">
                  <div class="col-lg-6">
                      <div class="row attraction-text-container">
                          <div class="row text-container">
                              <h5 class="attract-element"> Get your</h5>
                              <h5 class="attract-element"> E-commerce website</h5>
                              <h5 class="attract-element">up and running in 1 week</h5>
                          </div>
                          <h5 class="join-text">Join over 30 businesses that have made it.</h5>
                          <div class="get-started-btn-container">
                            <a class="default-btn get-started-btn">
                                  Get Started
                            </a>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="row header-image-container">
                          <div 
                          class="image-container"
                          >
                            <img 
                            src='https://res.cloudinary.com/hehe/image/upload/v1550747852/cart-site/attract.png'
                            />
                          </div>
                      </div>
                  </div>
              </div>
              <div class="row actions-container">
                    <span class='action-item'>
                        <i class='icon-start'></i>
                        <h5>Start</h5>
                        <i class='icon-angle-down'></i>
                    </span>
                    <span class='action-item'>
                        <i class='icon-clients'></i>
                        <h5>Clients</h5>
                        <i class='icon-angle-down'></i>
                    </span>
                    <span class='action-item'>
                        <i class='icon-testimonies'></i>
                        <h5>Testimonials</h5>
                        <i class='icon-angle-down'></i>
                    </span>
              </div>
        </div>
    </div>

    <div class="row simple-content-container containerized">
        <div class="col-lg-12">
            <div class="row onboarding-header-container">
                <h5>It is simple</h5>
                <h5>Get started for as low as RWf 25,000 / $40</h5>
            </div>
            <div class="row onboarding-steps-container">
                <div class="col-lg-6">
                    <div class="row steps-content">
                        <ul>
                          <li>Gather your business information</li>
                          <li>List down your products</li>
                          <li>Choose a domain name</li>
                          <li>Gather your product pictures</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class='row emphasize-container'>
                      <h5>We take care of all the rest</h5>
                      <a>Get started</a>
                    </div>
                    <div class='row contact-container'>
                      <h5>We contact you in less than 48 hours</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row showcase-container">
        <div class="big-image-container">
            <img 
              src='https://res.cloudinary.com/hehe/image/upload/v1550784482/cart-site/DSC00007.png'
            />
        </div>
        <div class="small-image-container">
          <img 
            src='https://res.cloudinary.com/hehe/image/upload/v1550784468/cart-site/Macbook-online_pay.png'
          />
        </div>
    </div>

    <div class="row shop-container">
        <div class="row title-container">
          <h5>Get your Products accessible</h5>
          <h5>to anyone, anytime, anywhere</h5>
          <h5>Over 4000 people are buying these products everyday</h5>
        </div>
        <div class='row slide-container'>
          <div id="products-slider-container" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
              <div class="carousel-item active">
                    <div class="row">
                      <div class="col-md-3">
                          <div class="row image-row">
                            <a href="#" class="product-image-container">
                              <img 
                              src='https://res.cloudinary.com/hehe/image/upload/v1550784468/cart-site/Group_41.png'
                              />
                            </a>
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="row image-row">
                            <a href="#" class="product-image-container">
                              <img 
                              src='https://res.cloudinary.com/hehe/image/upload/v1550784462/cart-site/Group_42.png'
                              />
                            </a>
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="row image-row">
                            <a href="#" class="product-image-container">
                              <img 
                              src='https://res.cloudinary.com/hehe/image/upload/v1550784461/cart-site/Group_43.png'
                              />
                            </a>
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="row image-row">
                            <a href="#" class="product-image-container">
                              <img 
                              src='https://res.cloudinary.com/hehe/image/upload/v1550784468/cart-site/Group_41.png'
                              />
                            </a>
                          </div>
                      </div>
                    </div>
              </div>
              <div class="carousel-item">
                    <div class="row">
                      <div class="col-md-3">
                          <div class="row image-row">
                            <a href="#" class="product-image-container">
                              <img 
                              src='https://res.cloudinary.com/hehe/image/upload/v1550784468/cart-site/Group_41.png'
                              />
                            </a>
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="row image-row">
                            <a href="#" class="product-image-container">
                              <img 
                              src='https://res.cloudinary.com/hehe/image/upload/v1550784468/cart-site/Group_41.png'
                              />
                            </a>
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="row image-row">
                            <a href="#" class="product-image-container">
                              <img 
                              src='https://res.cloudinary.com/hehe/image/upload/v1550784468/cart-site/Group_41.png'
                              />
                            </a>
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="row image-row">
                            <a href="#" class="product-image-container">
                              <img 
                              src='https://res.cloudinary.com/hehe/image/upload/v1550784468/cart-site/Group_41.png'
                              />
                            </a>
                          </div>
                      </div>
                    </div>
              </div>
            </div>
            <a class="carousel-control-prev" href="#products-slider-container" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#products-slider-container" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>

        <div class='row products-actions-container'>
          <button class='default-btn view-more-btn'>
            View more
          </button>
          <button class='default-btn get-started-btn'>
            Get started
          </button>
        </div>
    </div>



    @include('partials.client')


    <div class="row contact-us-container simple-content-container containerized">
        <div class="col-lg-12">
              <div class="row onboarding-header-container">
                  <h5>Get Started</h5>
                  <h5>We contact you in less than 48hours</h5>
              </div>
              <div class="row onboarding-steps-container">
                  <form class="form-container">
                      <div class="col-lg-12">
                        <input name="user-name" type="text" placeholder='Name' />
                        <input name="user-phone" type="text" placeholder='Phone'/>
                        <input name="user-email" type="email" placeholder='Your email' />
                      </div>
                      <div class="col-lg-12">
                          <div class="row submit-button-container">
                            <button type='button' class='default-btn submit-btn'>
                              Submit
                            </button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
    </div>

    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
    <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
  </body>
</html>
