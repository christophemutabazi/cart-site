<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    <div class='row error-page-container'>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="row error-page-header">
              <div class="col-lg-6 col-md-6 col-sm-12 col-4">
                <div class="row logo-container">
                  <a href="https://hehe.rw">
                    <img 
                      className="logo-image"
                      src="https://res.cloudinary.com/hehe/image/upload/v1550746033/cart-site/cart-icon.png"
                      />
                  </a>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-8">
                <div class="row button-container">
                  <a href="https://hehe.rw" class="default-btn homepage-link-btn">
                    Go to homepage
                  </a>
                </div>
              </div>
          </div>
          <div class="row error-message-container">
            <div class="col-lg-12"> 
                <div class="row content">
                  <div class="error-image-container">
                      <img 
                      src="https://res.cloudinary.com/hehe/image/upload/v1551131218/cart-site/products/404error.svg" 
                      />
                  </div>
                  <h5>Oops! You're lost...</h5>
                  <p>
                  The page you are loooking for might have been moved,
                  renamed, or does not exist.
                  </p>
                  <a href="https://hehe.rw">
                    Go to homepage <i class="icon-Arrow"></i>
                  </a>
                <div>
            </div>
          </div>
        </div>
    </div>

    @php do_action('get_footer') @endphp
    @php wp_footer() @endphp
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <!-- JavaScript -->
    <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
    <script src="https://res.cloudinary.com/hehe/raw/upload/v1551195415/cart-site/libraries/jquery.waypoints.min.js"></script>
  </body>
</html>
