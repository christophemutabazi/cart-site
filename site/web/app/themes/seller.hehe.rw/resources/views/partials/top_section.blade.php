@php
    $homepage_image_url = get_field('homepage_image_url'); 
    $shop_online_button_text = get_field('shop_online_button_text');
    $shop_online_button_url = get_field('shop_online_button_url');
@endphp
<div class="row top-container">
        @include('partials.header', [
            'menu_items' => $menu_items
        ])
        <div class="row jumbotron-container">
              <div class="row center-container">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <div class="row attraction-text-container">
                          <div class="row text-container">
                              <h5 class="attract-element">{{ $welcome_text_content['first_text'] }}</h5>
                              <h5 class="attract-element"> {{ $welcome_text_content['second_text'] }}</h5>
                              <h5 class="attract-element">{{ $welcome_text_content['third_text'] }}</h5>
                          </div>
                          <h5 class="join-text">{{ $subtitle }}</h5>
                          <div class="row get-started-btn-container">
                            <a 
                            href="#contactUs" 
                            class="default-btn get-started-btn">
                                  {{ $section_button['button_text'] }}
                            </a>
                            <a 
                            href="{{ $shop_online_button_url }}" 
                            class="default-btn shop-online-btn"
                            target="_blank"
                            >
                                  {{ $shop_online_button_text }}
                            </a>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <div class="row header-image-container">
                          <div 
                          class="image-container img-loader"
                          data-src="{{ $homepage_image_url }}"
                          >
                          <div class='image-placeholder'></div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="row actions-container">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                        <a href="#simpleContainer" class='action-item'>
                            <i class='icon-start'></i>
                            <h5>{{ $action_buttons['start_button_text'] }}</h5>
                            <i class='hide icon-Arrow'></i>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                        <a href="#shopContainer"class='action-item'>
                            <i class='icon-clients'></i>
                            <h5>{{ $action_buttons['clients_button_text'] }}</h5>
                            <i class='hide icon-Arrow'></i>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                        <a href="#clientContainer" class='action-item'>
                            <i class='icon-testimonies'></i>
                            <h5>{{ $action_buttons['testimonials_button_text'] }}</h5>
                            <i class='hide icon-Arrow'></i>
                        </a>
                    </div>
              </div>
        </div>
    </div>