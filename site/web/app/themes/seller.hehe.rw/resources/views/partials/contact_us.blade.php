<div class="row contact-us-container simple-content-container containerized diz waiting" id="contactUs">
    <div class="col-lg-12">
            <div class="row onboarding-header-container">
                <h5>{{ $title }}</h5>
                <h5>{{ $subtitle }}</h5>
            </div>
            <div class="row onboarding-steps-container">
                <div class="success feedback-message-container">
                    <h5>{{ $success_message }}</h5>
                </div>
                <div class="error feedback-message-container">
                    <h5>{{ $error_message }}</h5>
                </div>
                <form 
                class="form-container"
                data-receivers="{{ json_encode($email_receivers) }}"
                data-success="{{ $success_message }}"
                data-error="{{ $error_message }}"
                >
                    <div class="col-lg-12">
                    <input 
                    name="user_name"
                    class='user-name' 
                    type="text" 
                    placeholder="{{ $form_fields['name_form_field_placeholder'] }}" 
                    />
                    <input 
                    name="user_phone" 
                    class='user-phone'
                    type="text" 
                    placeholder="{{ $form_fields['phone_form_field_placeholder'] }}" 
                    />
                    <input 
                    name="user_email" 
                    class='user-email'
                    type="email" 
                    placeholder="{{ $form_fields['email_form_field_placeholder'] }}"  
                    />
                    <select class="country-selector hidden" 
                    name="user_country"
                    class='user-country'
                    id="userC"
                    data-placeholder="{{ $form_fields['country_form_field_placeholder'] }}"
                    >
                        <option></option>
                        
                    </select>
                    <select class="industry-selector hidden" 
                    name="user_industry"
                    class='user-industry'
                    id="userI"
                    data-placeholder="{{ $form_fields['industry_form_field_placeholder'] }}"
                    data-industry="{{ json_encode($industry_data) }}"
                    >
                        <option></option>
                        
                    </select>
                    </div>
                    <div class="col-lg-12">
                        <div class="row submit-button-container">
                        <button type='button' class='default-btn submit-btn'>
                            {{ $submit_button_text }}
                        </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
</div>