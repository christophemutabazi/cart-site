<footer class="row main-footer waiting" id="mainFooter">
    <div class="contactInfoSection">
        <div class="row contInfo">
            @foreach($footer_section_data as $section)
                <div class="help">
                    <span class="infotitle">{{ $section['title'] }}</span>
                    <span class="infoDetails">
                        <?php echo $section['content']['first_text'] ?>
                        <br />
                        <?php echo $section['content']['second_text'] ?>
                    </span>
                </div>
            @endforeach
        </div>
        <div class="row socialMedia">
            @foreach($social_media_contacts as $social_media_contact)
                <a 
                href="{{ $social_media_contact['social_media_page_link'] }}"
                target="_blank"
                >
                    <span class="{{ $social_media_contact['social_media_icon_class'] }}"></span>
                </a>
            @endforeach
        </div>
    </div>
    <div class="row bottomLink">
      @foreach(get_field('menu_item') as $menu_item)
        <a 
        href="{{ $menu_item['menu_section_link'] }}"
        target="_blank"
        >
            {{ $menu_item['menu_text'] }}
        </a>
      @endforeach
    </div>
</footer>
