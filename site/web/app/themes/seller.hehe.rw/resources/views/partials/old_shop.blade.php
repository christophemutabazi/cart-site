<div class="row shop-container">
      <div class="row title-container">
        <h5>{{ $title_one }}</h5>
        <h5>{{ $title_two }}</h5>
        <h5>{{ $subtitle }}</h5>
      </div>
      <div class='row slide-container'>
        <div id="products-slider-container" class="carousel slide" data-ride="carousel" data-interval="false">
          @php 
            $slides = count($products);
          @endphp
          <ol class="carousel-indicators">
            @for($i = 0; $i < $slides; $i++)
              <li data-target="#products-slider-container" data-slide-to="{{ $i }}" class="{{ $i == 0 ? 'active' : '' }}"></li>
            @endfor
          </ol>
          <div class="carousel-inner">
            @foreach($products as $product)
                <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                  <div class="row">
                    @foreach($product['product_item'] as $product_data)
                      <div class="col-md-3 col-sm-12 col-xs-12">
                          <div class="row image-row">
                            <a href="{{ $product_data['product_image_url'] }}" class="product-image-container">
                              <img 
                              src="{{ $product_data['product_image_url'] }}"
                              />
                            </a>
                          </div>
                      </div>
                    @endforeach
                  </div>
                </div>
            @endforeach
          </div>
          <a class="carousel-control-prev" href="#products-slider-container" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#products-slider-container" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>

      <div class='row products-actions-container'>
        <a href="{{ $view_more_btn['button_link'] }}" target='_blank' class='default-btn view-more-btn'>
          {{ $view_more_btn['button_text'] }}
        </a>
        <a href="{{ $get_started_btn['button_link'] }}" class='default-btn get-started-btn'>
          {{ $get_started_btn['button_text'] }}
        </a>
      </div>
  </div>