<div class='row client-container diz waiting' id="clientContainer">

    <div class='col-lg-12 col-md-12 col-sm-12 col-12'>
        <div class="row client-header-container">
            <h5>{{ get_field('section_title') }}</h5>
        </div>
    </div>

    <div class='col-lg-12 col-md-12 col-sm-12 col-12 reset-pad'>
        <div id="client-slide" 
        class="carousel slide carousel-fade" 
        data-ride="carousel"
        data-interval="10000"
        >
            <ol class="carousel-indicators">
                @for($i = 0; $i < count(get_field('testimony_item')); $i++)
                <li data-target="#client-slide" data-slide-to="{{ $i }}" class="{{ $i == 0 ? 'active' : '' }}"></li>
                @endfor
            </ol>
            <div class="carousel-inner">
                @foreach(get_field('testimony_item') as $testimony)
                <div class="carousel-item {{ $loop->first ? 'active' : ''}}">
                    <div class="row testimony-item-container">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 reset-pad">
                            <div 
                            class="testimony-client-image-container img-loader"
                            data-src="{{ $testimony['client_image'] }}"
                            >
                            <div class='image-placeholder'></div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 reset-pad">
                            <div class="row testimony">
                                <div class='container-center'>
                                    <a 
                                    class='client-company-name'
                                    href="{{ $testimony['client_company_info']['shop_link'] }}"
                                    target="_blank"
                                    >
                                    {{ $testimony['client_company_info']['company_name'] }}
                                    </a>
                                    <p>{{ strip_tags($testimony['content']) }}</p>
                                    <div class='client-name-container'>
                                        <span>{{ $testimony['client_name'] }}</span>
                                        <span>{{ $testimony['client_position'] }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div 
                    class="client-platform-image-container img-loader"
                    data-src="{{ $testimony['platform_image_url'] }}"
                    >
                    <div class='image-placeholder not-full'></div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    
</div>
