<section class="row simple-content-container containerized waiting" id="simpleContainer">
        <div class="col-lg-12">
            <!-- <div class="row onboarding-header-container">
                <h5>{{ $title }}</h5>
                <h5>{{ $subtitle }}</h5>
            </div>
            <div class="row onboarding-steps-container">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="row steps-content">
                        <ul>
                          @foreach ($steps as $step)
                            <li>{{ $step['step_content'] }}</li>
                          @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class='row emphasize-container'>
                      <h5>We take care of all the rest</h5>
                      <a href="#contactUs">{{ $button_text }} <i class='icon-Arrow'></i></a>
                    </div>
                    <div class='row contact-container'>
                      <h5>{{ $contact_text }}</h5>
                    </div>
                </div>
            </div> -->
            <div class="pricing-section">
    <div class="content-wrapper">
        <div class="section-title">
            <h3>Pricing Plan</h3>
        </div>
        <div class="table-content">
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th>Freemium<span>Expires after 3 months</span></th>
                        <th>Premium<span>Marketplace</span></th>
                        <th>Exclusive<span>Marketplace + Storefront</span></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Marketplace Product Listing</td>
                        <td>Free &amp; Unlimited</td>
                        <td>Free &amp; Unlimited</td>
                        <td>Free &amp; Unlimited</td>
                    </tr>

                    <tr>
                        <td>Product Photography</td>
                        <td>500 Rwf / product</td>
                        <td>500 Rwf / product</td>
                        <td>500 Rwf / product</td>
                    </tr>

                    <tr>
                        <td>Random</td>
                        <td>Promoted</td>
                        <td>Promoted</td>
                        <td>Promoted</td>
                    </tr>

                    <tr>
                        <td>Digital Marketing</td>
                        <td>Basic SEO</td>
                        <td>Flexible Budget <span>10% Service Fee off your proposed budget</span></td>
                        <td>Flexible Budget <span>10% Service Fee off your proposed budget</span></td>                                
                    </tr>

                    <tr>
                        <td>Commision on Sales</td>
                        <td>-</td>
                        <td>7% <span>On Sales > 2M Rwf annually</span></td>
                        <td>None</td>
                    </tr>

                    <tr>
                        <td>Shipping</td>
                        <td>Standard</td>
                        <td>Standard &amp; Express</td>
                        <td>Standard &amp; Express</td>
                    </tr>

                    <tr>
                        <td>Global Selling <span>(Starting July 2019)</span></td>
                        <td>-</td>
                        <td>Included</td>
                        <td>Included</td>
                    </tr>

                    <tr>
                        <td>Warehouse Storage</td>
                        <td>-</td>
                        <td>Included<span>Rwf 20,000/sqm</span></td>
                        <td>Included<span>Rwf 20,000/sqm</span></td>
                    </tr>

                    <tr>
                        <td>Quality Assurance</td>
                        <td>Weekly</td>
                        <td>Daily</td>
                        <td>On-demand <span>(Rwf 5000/day)</span></td>
                    </tr>

                    <tr>
                        <td>Digital Payment Integration</td>
                        <td>Included</td>
                        <td>Included</td>
                        <td>Included + Rwf 50,000</td>
                    </tr>

                    <tr>
                        <td>Free Training Sessions <span>Extra support at Rwf 5000/day</span></td>
                        <td>3</td>
                        <td>5</td>
                        <td>5</td>
                    </tr>

                    <tr>
                        <td>Domain Name</td>
                        <td>-</td>
                        <td>-</td>
                        <td>Rwf 12,000</td>
                    </tr>

                    <tr>
                        <td>Monthly Maintenance Fee</td>
                        <td>-</td>
                        <td>-</td>
                        <td>Rwf 1,200,000/year</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="button-container">
            <a href="#contactUs">Get Started</a>
            <a href="#contactUs">Try Freemium</a>
        </div>
    </div>				
</div>
        </div>
</section>

<div class="row showcase-container diz waiting" id="showCaseContainer">
    <div 
    class="big-image-container img-loader"
    data-src="https://res.cloudinary.com/hehe/image/upload/v1550784482/cart-site/DSC00007.png"
    >
    <div class='image-placeholder'></div>
    </div>
    <div 
    class="small-image-container img-loader"
    data-src="https://res.cloudinary.com/hehe/image/upload/v1550784468/cart-site/Macbook-online_pay.png"
    >
    <div class='image-placeholder'></div>
    </div>
</div>