<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
  <title>
    HeHe | Cart
  </title>
  <link rel="icon" 
      type="image/png" 
      href="https://res.cloudinary.com/hehe/image/upload/v1551450144/cart-site/hcart_favico_d2O_icon.ico">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <!-- CSS -->
  <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
  @php wp_head() @endphp
</head>
