<div class="row shop-container waiting" id="shopContainer">
      <div class="row title-container">
        <h5>{{ $title_one }}</h5>
        <h5>{{ $title_two }}</h5>
        <h5>{{ $subtitle }}</h5>
      </div>
      <div class='row slide-container'>
        <div class="carousel" data-flickity='{ "imagesLoaded": true, "percentPosition": true, "wrapAround": true, "groupCells": true }'>
          @foreach($products as $product)
            <div class="row product-item-container">
                <div class="product-image-container">
                  <a 
                  href="{{ $product['product_shop_url'] }}"
                  class="img-loader"
                  target='_blank'
                  data-src="{{ $product['product_image_url'] }}"
                  >
                    <div class='image-placeholder'></div>
                  </a>
                </div>
                <div class="products-image">
                    <span
                    title="{{ $product['product_name'] }}"
                    >
                      {{
                        strlen($product['product_name']) > 15 ? 
                        substr($product['product_name'], 0, 12) . '...' :
                        $product['product_name']
                      }}
                    </span>
                    <span>{{ $product['product_price'] }}</span>
                </div>
            </div>
          @endforeach
        </div>
      </div>
      <div class='row products-actions-container'>
        <a href="{{ $view_more_btn['button_link'] }}" target='_blank' class='default-btn view-more-btn'>
          {{ $view_more_btn['button_text'] }}
        </a>
        <a href="#contactUs" class='default-btn get-started-btn'>
          {{ $get_started_btn['button_text'] }}
        </a>
      </div>
</div>