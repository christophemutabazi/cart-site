@php 
  $get_started_button = get_field('get_started_button');
@endphp 
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="https://hehe.rw">
    <img 
    className="logo-image"
    src="https://res.cloudinary.com/hehe/image/upload/v1550746033/cart-site/cart-icon.png"
    />
  </a>
  <button class="navbar-toggler" type="button">
    <span class="icon-Bugger"></span>
  </button>
  <div class="collapse navbar-collapse" id="main-nav">
    <button class='menu-closer'>
      <span class="icon-close"></span>
    </button>
    <ul class="navbar-nav mr-auto">
      @foreach($menu_items as $menu_item)
        <li class="nav-item">
          <a class="nav-link" href="{{ $menu_item['menu_section_link'] }}">{{ $menu_item['menu_text'] }}</a>
        </li>
      @endforeach
      <li>
        <a class='default-btn get-started-header-btn' href="{{ $get_started_button['button_link'] }}">
          {{ $get_started_button['button_text'] }}
        </a>
      </li>  
    </ul>
  </div>
</nav>
