{{--
  Template Name: Career Template
--}}
<?php 
// header content data
$menu_items = get_field('menu_item');

$job_title = get_field('job_title');
$who_you_are = get_field('who_you_are');
$intro = get_field('intro');
$roles_title = get_field('roles_title');
$responsibilities = get_field('responsibilities');
$experience_title = get_field('experience_title');
$experiences = get_field('experience_content');
$conclusion_text = get_field('conclusion_text');
$apply_job_link = get_field('apply_job_link');
//var_dump($responsibilities);
// var_dump($intro);
//die();
// footer section data
$footer_section_data = get_field('section_contact_item');
$social_media_contacts = get_field('social_media_contacts');
?>
<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    <div class="row top-container career-container">
      @include('partials.header', [
            'menu_items' => $menu_items
        ])

        <div class="row vacancy-container">
            <div class="row applicant-intro">
              <h5 class="job-title">{{ $job_title }}</h5>
              <h5 class="intro-title">{{ $who_you_are }}</h5>
              <?php  echo $intro ?>
            </div>
            <div class="row applicant-roles">
              <h5>{{ $roles_title }}</h5>
              <ul>
                @foreach($responsibilities as $responsibility) 
                  <li>{{ $responsibility['single_responsibility'] }}</li>
                @endforeach
              </ul>
            </div>
            <div class="row applicant-roles">
              <h5>{{ $experience_title }}</h5>
              <ul>
                @foreach($experiences as $experience)
                  <li>{{ $experience['single_experience'] }}</li>
                @endforeach
              </ul>
            </div>
            <div class="row company-conclusion">
                <?php echo $conclusion_text ?>
                <a href="{{ $apply_job_link }}" class='apply-btn'>
                  Apply for this job
                </a>
            </div>
        </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer', [
    'footer_section_data' => $footer_section_data,
    'social_media_contacts' => $social_media_contacts
    ])
    @php wp_footer() @endphp
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <!-- JavaScript -->
    <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
    <script src="https://res.cloudinary.com/hehe/raw/upload/v1551195415/cart-site/libraries/jquery.waypoints.min.js"></script>
  </body>
</html>
