export default {
  init() {
    // JavaScript to be fired on all pages
    // open mobile hamburger button
    $('.navbar-toggler').on('click', () => {
      $('#main-nav').addClass('prep');
      $('#main-nav').addClass('show');
      setTimeout(() => {
        $('#main-nav').addClass('fade-in');
      }, 100);
      $('.menu-closer').addClass('show');
      $('html').addClass('removeOverflow');
    });

    // close mobile hamburger button

    $('.menu-closer').on('click', () => {
      $('#main-nav').removeClass('fade-in');
      setTimeout(() => {
        $('#main-nav').removeClass('show');
        $('#main-nav').removeClass('prep');
        $('.menu-closer').removeClass('show');
        $('html').removeClass('removeOverflow');
      }, 500);
    });





  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
