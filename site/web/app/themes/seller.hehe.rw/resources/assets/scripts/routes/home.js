export default {
  init() {
    $(document).ready(() => {
      $('a[href*="#"]').on('click', function (e) {
        e.preventDefault();
        var offsetRemoval = 100;
        if ($(this).attr('href') === '#shopContainer') {
          offsetRemoval = 110;
        }
        if ($('#main-nav').hasClass('fade-in')) {
          $('#main-nav').removeClass('fade-in');
          setTimeout(() => {
            $('#main-nav').removeClass('show');
            $('#main-nav').removeClass('prep');
            $('.menu-closer').removeClass('show');
            $('html').removeClass('removeOverflow');
            $('html, body').animate({
              scrollTop: $($(this).attr('href')).offset().top - offsetRemoval,
            }, 500, 'linear');
          }, 500);
        } else {
          $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top - offsetRemoval,
          }, 500, 'linear');
        }
      });

      // comin effect
      var wayP = new Waypoint({
        element: $('#simpleContainer'),
        handler: function () {
          $('#simpleContainer').addClass('animated');
          $('#showCaseContainer').addClass('animated');
        },
        offset: '90%',
      });

      var wayP3 = new Waypoint({
        element: $('#shopContainer'),
        handler: function () {
          $('#shopContainer').addClass('animated');
        },
        offset: '90%',
      });

      var wayP5 = new Waypoint({
        element: $('#clientContainer'),
        handler: function () {
          $('#clientContainer').addClass('animated');
        },
        offset: '90%',
      });

      var wayP6 = new Waypoint({
        element: $('#contactUs'),
        handler: function () {
          $('#contactUs').addClass('animated');
        },
        offset: '90%',
      });

      var wayP7 = new Waypoint({
        element: $('#mainFooter'),
        handler: function () {
          $('#mainFooter').addClass('animated');
        },
        offset: '90%',
      });

      // load images functionality

      // provide the screen size
      function getDeviceState() {
        var style = window.getComputedStyle(document.querySelector('#state-indicator'), ':before');
        return style.getPropertyValue('content');
      }

      function addCustomOptimizations(imageUrl) {
        let finalImageUrl = imageUrl.split('/');
        let uploadStringIndex = finalImageUrl.indexOf('upload');
        if (uploadStringIndex === -1) {
          return finalImageUrl.join('/');
        }
        finalImageUrl[uploadStringIndex] = `${finalImageUrl[uploadStringIndex]}/q_auto,f_auto,fl_lossy`;
        if (getDeviceState() === 'none' || getDeviceState() === undefined || getDeviceState() === null) {
          return finalImageUrl.join('/');
        }
        const screenSize = getDeviceState();
        switch (screenSize) {
          case '"tablet"':
              finalImageUrl[uploadStringIndex] = `${finalImageUrl[uploadStringIndex]},w_768,c_scale`;
              return finalImageUrl.join('/');
          case '"tablet-small"':
              finalImageUrl[uploadStringIndex] = `${finalImageUrl[uploadStringIndex]},w_480,c_scale`;
              return finalImageUrl.join('/');
          case '"mobile-min"':
              finalImageUrl[uploadStringIndex] = `${finalImageUrl[uploadStringIndex]},w_320,c_scale`;
              return finalImageUrl.join('/');
          default:
              return finalImageUrl.join('/');
        }
      }

      function loadImages() {
        $('.img-loader').each(function () {
          var container = $(this);
          var image_url = container.attr('data-src');
          const final_url = addCustomOptimizations(image_url);
          var Img = new Image();
          Img.onload = function () {
            var __self = this;
            var image_html = '<img src="' + __self.src + '">';
            var image_placeholder = container.find('.image-placeholder');
            image_placeholder.addClass('hidden');
            container.append(image_html);
            setTimeout(() => {
              container.addClass('loaded');
              image_placeholder.remove();
            }, 100);
          };
          Img.src = final_url;
        });
      }

      setTimeout(() => {
        loadImages();
      }, 1000);


    });



    // render country selector
    const selectorData = [];
    $('.country-selector').select2({
      placeholder: `${$('.country-selector').attr('data-placeholder')}`,
      data: [],
    });
    $.ajax({
      url: 'https://restcountries.eu/rest/v2/all',
      method: 'GET',
      success: (data) => {
        if (data) {
          for (let i = 0; i < data.length; i++) {
            selectorData.push({
              id: data[i].name,
              text: data[i].name,
            });
          }
          $('.country-selector').select2({
            placeholder: `${$('.country-selector').attr('data-placeholder')}`,
            data: selectorData,
          });
        }
      },
    });

    // render industry selector
    $('.industry-selector').select2({
      placeholder: `${$('.industry-selector').attr('data-placeholder')}`,
      data: [],
    });
    let industry_data = $('.industry-selector').attr('data-industry');
    if (industry_data !== undefined) {
      industry_data = JSON.parse(industry_data);
      const formattedData = [];
      for (let i = 0; i < industry_data.length; i++) {
        formattedData.push({
          id: industry_data[i].industry_name,
          text: industry_data[i].industry_name,
        });
      }

      formattedData.push({
        id: 'other',
        text: 'other',
      });


      $('.industry-selector').select2({
        placeholder: `${$('.industry-selector').attr('data-placeholder')}`,
        data: formattedData,
      });
    }

    function validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }

    function inputValidations() {
      const errors = {};
      if ($('.user-name').val() === '') {
        errors.user_name = 'The name is required';
      }

      if ($('.user-phone').val() === '') {
        errors.user_phone = 'The phone number is required';
      }

      if ($('.user-email').val() === '') {
        errors.user_email = 'The email is required';
      } else if (!validateEmail($('.user-email').val())) {
        errors.user_email = 'The email you provided is invalid';
      }

      //console.log($('#userC').val());

      if ($('#userC').val() === '') {
        //console.log($('#userC').val());
        errors.user_country = 'The country is required';
      }

      if ($('#userI').val() === '') {
        errors.user_industry = 'The industry is required';
      }

      return errors;
    }

    function addErrorValidationMessages(errors) {
      if (errors.user_name) {
        if (!$('.user-name').hasClass('has-error')) {
          const errorMessage = `<span class='error-message'>${errors.user_name}</span>`;
          $('.user-name').after(errorMessage);
          $('.user-name').addClass('has-error');
        }
      }

      if (errors.user_phone) {
        if (!$('.user-phone').hasClass('has-error')) {
          const errorMessage = `<span class='error-message'>${errors.user_phone}</span>`;
          $('.user-phone').after(errorMessage);
          $('.user-phone').addClass('has-error');
        }
      }

      if (errors.user_email) {
        if (!$('.user-email').hasClass('has-error')) {
          const errorMessage = `<span class='error-message'>${errors.user_email}</span>`;
          $('.user-email').after(errorMessage);
          $('.user-email').addClass('has-error');
        }
      }

    }

    function hideErrorValidationMessages() {
      if ($('.user-name').hasClass('has-error')) {
        $('.user-name').next().remove();
        $('.user-name').removeClass('has-error');
      }
      if ($('.user-phone').hasClass('has-error')) {
        $('.user-phone').next().remove();
        $('.user-phone').removeClass('has-error');
      }
      if ($('.user-email').hasClass('has-error')) {
        $('.user-email').next().remove();
        $('.user-email').removeClass('has-error');
      }
    }

    function getEmailReceivers() {
      let emailReceivers = $('.form-container').attr('data-receivers');
      if (emailReceivers !== undefined) {
        emailReceivers = JSON.parse(emailReceivers);
        const receiversData = emailReceivers.map((receiver) => {
          return {
            email: receiver['staff_email'],
          }
        });
        return receiversData;
      }

      return undefined;
    }

    function showFeedBackMessageBox(context) {
      $(`.feedback-message-container.${context}`).addClass('show');
      setTimeout(() => {
        $(`.feedback-message-container.${context}`).addClass('fade-in');
      }, 200);

      setTimeout(() => {
        $(`.feedback-message-container.${context}`).removeClass('fade-in');
        setTimeout(() => {
          $(`.feedback-message-container.${context}`).removeClass('show');
        }, 300);
      }, 3000);
    }

    function resetFormFields() {
      $('.user-name').val('');
      $('.user-phone').val('');
      $('.user-email').val('');
      $('#userC').val('');
      $('#userI').val('');
    }

    //implement the contact us form functionality
    $('.submit-btn').on('click', () => {
      /**
       * To do perform inputs validations
       */
      const errors = inputValidations();
      if (!$.isEmptyObject(errors)) {
        // add validation messages
        addErrorValidationMessages(errors);
        setTimeout(() => {
          hideErrorValidationMessages();
        }, 3000);
        return;
      }

      // get receivers data
      const receiversData = getEmailReceivers();

      $('.submit-btn').html('Submitting...');

      const data = {
        receiver_emails: receiversData,
        user_name: $('.user-name').val(),
        user_phone: $('.user-phone').val(),
        user_email: $('.user-email').val(),
        user_country: $('#userC').val(),
        user_industry: $('#userI').val(),
      }

      $.ajax({
        url: 'https://heherw.com/api/customers/emails/send',
        method: 'POST',
        data: data,
        success: (response) => {
          if (response.status === 'success') {
            $('.submit-btn').html('Submit');
            showFeedBackMessageBox('success');
            resetFormFields();
          }

          if (response.status === 'failure') {
            $('.submit-btn').html('Submit');
            showFeedBackMessageBox('error');
          }
        },
        error: (XMLHttpRequest) => {
          /**
           * Handling different kinds of errors
           */
          if (XMLHttpRequest.readyState == 4) {
            // HTTP error (can be checked by XMLHttpRequest.status and XMLHttpRequest.statusText)
            $('.submit-btn').html('Submit');
            showFeedBackMessageBox('error');
          }
          else if (XMLHttpRequest.readyState == 0) {
            // Network error (i.e. connection refused, access denied due to CORS, etc.)
            $('.submit-btn').html('Submit');
            showFeedBackMessageBox('error');
          }
          else {
            // something weird is happening
            $('.submit-btn').html('Submit');
            showFeedBackMessageBox('error');
          }
        },
      });

    });



    // scroll to section functionality



  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
